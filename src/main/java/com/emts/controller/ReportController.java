package com.emts.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.emts.model.AdvertisementTracking;
import com.emts.model.Chanel;
import com.emts.model.Client;
import com.emts.model.Party;
import com.emts.model.PieChart;
import com.emts.model.PrintTracking;
import com.emts.model.Publication;
import com.emts.model.Tracking;
import com.emts.service.AdvReportService;
import com.emts.service.BarChartService;
import com.emts.service.ChanelService;
import com.emts.service.ClientService;
import com.emts.service.PrintReportService;
import com.emts.service.PublicationService;
import com.emts.service.ReportService;
import com.emts.util.DateFormat;
import com.emts.validator.PieChartValidator;

@Controller
public class ReportController {
	
@Autowired 
private ReportService reportService;

@Autowired 
private PrintReportService printReportService;

@Autowired 
private AdvReportService advReportService;
	
@Autowired 
private BarChartService barChartService;

@Autowired
private ClientService clientService;

@Autowired 
private ChanelService chanelService;

@Autowired
private PublicationService publicationService;

@Autowired
private PieChartValidator pieChartValidator;

HttpSession session;
/*@Autowired
private PieChartValidator pieChartValidator;*/
 
/*Pie Chart for Electronic Media Tracking*/

	@RequestMapping("/piechart")
	public String showPiechart(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Chanel> chanelList=new ArrayList<Chanel>();
		chanelList=chanelService.getAllChanel();
		if(chanelList!=null)
		{
			model.addAttribute("chanel", chanelList);
		}
		return "piechart";
	}
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/PieChartReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String genrateReport(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param,@RequestParam(required=false) Integer clientId,@RequestParam(required=false) String todate,@RequestParam(required=false) String fromdate,@RequestParam(required=false) Integer channelId ) 
	{
		System.out.println("Param value"+clientId+todate);
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Chanel> chanelList=new ArrayList<Chanel>();
		chanelList=chanelService.getAllChanel();
		if(chanelList!=null)
		{
			model.addAttribute("chanel", chanelList);
		}
		/*All Channel and Individual Channel Negative Positive Report(Electronic Media Tracking)*/
		
		if(param!=null && param.equals("{param=Channel}"))
		{
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		HashMap<String, Object[]> map=reportService.pieChartReport(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getChanel().getChannelId(),param);
		pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
		pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
		Set  set = map.keySet();
             Iterator  it = set.iterator();
             int i=1;
             while(it.hasNext())
             {
            	Object[] obj=(Object[]) map.get(it.next());
            	@SuppressWarnings("unchecked")
				ArrayList list = new ArrayList(Arrays.asList(obj));
            	model.addAttribute("obj"+i++,list);
            	model.addAttribute("pieChart",pieChart);
                model.addAttribute("clientName",clientName);
            	
             }
             return "generateAllChannelPieCharts";  
             
		}
		if(param!=null && param.equals("{param=AllChannel}"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(pieChart.getClient().getClientId());
			HashMap<String, Object[]> map=reportService.pieChartReport(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),null, param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			Set  set = map.keySet();
	             Iterator  it = set.iterator();
	             int i=1;
	             while(it.hasNext())
	             {
	            	Object[] obj=(Object[]) map.get(it.next());
	            	@SuppressWarnings("unchecked")
					ArrayList list = new ArrayList(Arrays.asList(obj));
	            	model.addAttribute("obj"+i++,list);
	            	model.addAttribute("pieChart",pieChart);
	            	model.addAttribute("clientName",clientName);
	             }
	             return "generateAllChannelPieCharts";  
		}
		/*Sector Wise All Channel Negative Positive Report(Electronic Media Tracking)*/
		if(param!=null && param.equals("{param=Sector}"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(pieChart.getClient().getClientId());
			HashMap<String, List<Object>> sectorMap= reportService.genrateSectorWisePieChartReport(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate())); 
			Set  set = sectorMap.keySet();
             Iterator  it = set.iterator();
             while(it.hasNext())
             {
            	 List<Object> sectorPositiveList= (List<Object>)sectorMap.get(it.next());
            	 List<Object> sectorNegativeList= (List<Object>)sectorMap.get(it.next());
            	 List<Object> channelListList= (List<Object>)sectorMap.get(it.next());
            	 model.addAttribute("sectorPositiveList",sectorPositiveList);
            	 model.addAttribute("sectorNegativeList",sectorNegativeList);
            	 model.addAttribute("channelListList",channelListList);
            	 model.addAttribute("pieChart",pieChart);
            	 model.addAttribute("clientName",clientName);
             }
			
			return "generateElectronicMediaSectorWisePieChart";
		}
		/* Channel Wise Negative Positive Report(Electronic Media Tracking)*/
		if(param!=null && param.equals("{param=AllChannelNegativePositive}"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(pieChart.getClient().getClientId());
			System.out.println("AllChannelNegativePositive");
			HashMap<String, List<Object>> map = reportService.channelWisePieChart(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> ChannelPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelNegativeList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	
           	 model.addAttribute("ChannelPositiveList",ChannelPositiveList);
           	 model.addAttribute("ChannelNegativeList",ChannelNegativeList);
           	 model.addAttribute("ChannelList",ChannelList);
           	 model.addAttribute("pieChart",pieChart);
           	 model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
           	model.addAttribute("clientName",clientName);
           
           	session = request.getSession();
           	session.setAttribute("ChannelPositiveList",ChannelPositiveList);
        	session.setAttribute("ChannelNegativeList",ChannelNegativeList);
           	
			}
			return "generateChannelWisePieChart";
		}
		if(param!=null && param.equals("AllChannelNegativePositive1"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			HashMap<String, List<Object>> map = reportService.channelWisePieChart(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),"{param=AllChannelNegativePositive}");
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> ChannelPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelNegativeList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	
           	 model.addAttribute("ChannelPositiveList",ChannelPositiveList);
           	 model.addAttribute("ChannelNegativeList",ChannelNegativeList);
           	 model.addAttribute("ChannelList",ChannelList);
           	 /*model.addAttribute("pieChart",pieChart);*/
           	model.addAttribute("todate",todate);
           	model.addAttribute("clientId",clientId);
           	model.addAttribute("fromdate",fromdate);
           	model.addAttribute("clientName",clientName);
			}
			return "generateChannelWisePieChart1";
		}
		if(param!=null && param.equals("AllChannelNegativePositive2"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			System.out.println("-----AllChannelNegativePositive2----");
			HashMap<String, List<Object>> map = reportService.channelWisePieChart(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),"{param=AllChannelNegativePositive}");
			/*pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));*/
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> ChannelPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelNegativeList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	
           	 model.addAttribute("ChannelPositiveList",ChannelPositiveList);
           	 model.addAttribute("ChannelNegativeList",ChannelNegativeList);
           	 model.addAttribute("ChannelList",ChannelList);
           	 /*model.addAttribute("pieChart",pieChart);*/
           	model.addAttribute("todate",todate);
           	model.addAttribute("clientId",clientId);
           	model.addAttribute("fromdate",fromdate);
        	model.addAttribute("clientName",clientName);
			}
			return "generateChannelWisePieChart";
		}
		if(param!=null && param.equals("AllChannelNegativePositive3"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			HashMap<String, List<Object>> map = reportService.channelWisePieChart(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),"{param=AllChannelNegativePositive}");
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> ChannelPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelNegativeList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	
           	 model.addAttribute("ChannelPositiveList",ChannelPositiveList);
           	 model.addAttribute("ChannelNegativeList",ChannelNegativeList);
           	 model.addAttribute("ChannelList",ChannelList);
           	 /*model.addAttribute("pieChart",pieChart);*/
           	model.addAttribute("todate",todate);
           	model.addAttribute("clientId",clientId);
           	model.addAttribute("fromdate",fromdate);
           	model.addAttribute("clientName",clientName);
			}
			return "generateChannelWisePieChart2";
		}
		if(param!=null && param.equals("AllChannelNegativePositive4"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			HashMap<String, List<Object>> map = reportService.channelWisePieChart(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),"{param=AllChannelNegativePositive}");
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> ChannelPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelNegativeList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	
           	 model.addAttribute("ChannelPositiveList",ChannelPositiveList);
           	 model.addAttribute("ChannelNegativeList",ChannelNegativeList);
           	 model.addAttribute("ChannelList",ChannelList);
           	 /*model.addAttribute("pieChart",pieChart);*/
           	model.addAttribute("todate",todate);
           	model.addAttribute("clientId",clientId);
           	model.addAttribute("fromdate",fromdate);
           	model.addAttribute("clientName",clientName);
			}
			return "generateChannelWisePieChart3";
		}
		if(param!=null && param.equals("{param=ChannelSector}"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(pieChart.getClient().getClientId());
			HashMap<String, List<Object>> map=reportService.sectorWiseNegativePosReport(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getChanel().getChannelId(),param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> sectorPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorNegativeList= (List<Object>)map.get(iterator.next());
           	 model.addAttribute("sectorPositiveList",sectorPositiveList);
           	 model.addAttribute("sectorNegativeList",sectorNegativeList);
           	 model.addAttribute("sectorList",sectorList);
           	 model.addAttribute("pieChart",pieChart);
           	 model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
           	 model.addAttribute("channelId",channelId);
           	model.addAttribute("clientName",clientName);
         	session = request.getSession();
           	session.setAttribute("sectorPositiveList",sectorPositiveList);
        	session.setAttribute("sectorNegativeList",sectorNegativeList);
			}
			return "generateSectorWisePieCharts";
		}
		if(param!=null && param.equals("ChannelSector1"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			HashMap<String, List<Object>> map=reportService.sectorWiseNegativePosReport(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),channelId,"{param=ChannelSector}");
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> sectorPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorNegativeList= (List<Object>)map.get(iterator.next());
           	 model.addAttribute("sectorPositiveList",sectorPositiveList);
           	 model.addAttribute("sectorNegativeList",sectorNegativeList);
           	 model.addAttribute("sectorList",sectorList);
           	 model.addAttribute("pieChart",pieChart);
             model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
           	 model.addAttribute("channelId",channelId);
           	model.addAttribute("clientName",clientName);
			}
			return "generateSectorWisePieCharts1";
		}
		if(param!=null && param.equals("ChannelSector2"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			HashMap<String, List<Object>> map=reportService.sectorWiseNegativePosReport(clientId,DateFormat.getYYYYMMDDDate(fromdate),DateFormat.getYYYYMMDDDate(todate),channelId,"{param=ChannelSector}");
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> sectorPositiveList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorList= (List<Object>)map.get(iterator.next());
           	 List<Object> sectorNegativeList= (List<Object>)map.get(iterator.next());
           	 model.addAttribute("sectorPositiveList",sectorPositiveList);
           	 model.addAttribute("sectorNegativeList",sectorNegativeList);
           	 model.addAttribute("sectorList",sectorList);
           	 model.addAttribute("pieChart",pieChart);
             model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
           	 model.addAttribute("channelId",channelId);
         	model.addAttribute("clientName",clientName);
			}
			return "generateSectorWisePieCharts";
		}
		return "piechart";
	}
	
	/*PDF Report For channel(Electronic Media Tracking)*/
	@RequestMapping("/PDFReport")
	public String showPDFReport(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Chanel> chanelList=new ArrayList<Chanel>();
		chanelList=chanelService.getAllChanel();
		if(chanelList!=null)
		{
			model.addAttribute("chanel", chanelList);
		}
		return "PDFReport";
	}
	@RequestMapping(value = "/EMTorPDFReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String genratePDFReport(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ) 
	{
		pieChartValidator.validate(pieChart, result);
		if(result.hasErrors())
		{
			return "PDFReport";
		}
		@SuppressWarnings("rawtypes")
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		@SuppressWarnings("rawtypes")
		List channelName=new ArrayList();
		channelName=chanelService.getChannelName(pieChart.getChanel().getChannelId());
		System.out.println("PDF Report Action");
		List<Tracking> pdfTracking=new ArrayList<Tracking>();
		pdfTracking=reportService.EMTPDFReport(pieChart.getFromDate(),pieChart.getToDate(),pieChart.getChanel().getChannelId());
		if(pdfTracking!=null)
		{
			model.addAttribute("pdfTracking", pdfTracking);
			model.addAttribute("pieChart",pieChart);
			model.addAttribute("clientName",clientName);
			model.addAttribute("channelName",channelName);
		}
		return "generatePDFReport";
		
	}
	
	/* Generate Image Bar(BarChart)  */
	@RequestMapping("/barChart")
	public String showBarChart(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		return "barChart";
	}
	
	@RequestMapping(value = "/BarChartImage", method = {RequestMethod.GET,RequestMethod.POST })
	public String genrateBarChart(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ) 
	{
		@SuppressWarnings("rawtypes")
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		List<Object> barChartList=new ArrayList<Object>();
		barChartList=barChartService.barChart(pieChart.getFromDate(),pieChart.getToDate(),pieChart.getClient().getClientId());
		pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
		pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
		if(barChartList!=null)
		{
			@SuppressWarnings("rawtypes")
			List list= (List) barChartList.get(0); 
			System.out.println("List===="+list.size());
			model.addAttribute("dateList", barChartList.get(0));
			model.addAttribute("markingList", barChartList.get(1));
			model.addAttribute("channelList", barChartList.get(2));
			model.addAttribute("dateListSize", list.size());
			model.addAttribute("clientName", clientName);
			model.addAttribute("barChartList", barChartList);
			model.addAttribute("pieChart", pieChart);
		}
		return "generateBarChart";	
	}
	
/* Advertisement Pie Chart for Channel wise and All Channel*/	
	@RequestMapping("/advPieChart")
	public String showAdvPieChartReport(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Party> party=new ArrayList<Party>();
		party=advReportService.getAllParty();
		if(party!=null)
		{
			model.addAttribute("party", party);
		}
		return "advPieChart";
	}
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/ADVPieChartReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String genrateAdvPieChartReport(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ,@RequestParam(required=false) Integer clientId,@RequestParam(required=false) String todate,@RequestParam(required=false) String fromdate,@RequestParam(required=false) Integer partyId[]) 
	{
		if(param!=null && param.equals("{param=AllChennal}"))
		{
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		List<Object> advList=advReportService.AllChanelPieChart(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPartyId());
		pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
		pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
		model.addAttribute("advList",advList);
		model.addAttribute("clientName",clientName);
		model.addAttribute("pieChart", pieChart);
		return "generateAllChannelAdvReport";
		}
 		if(param!=null && param.equals("{param=Channel}"))
		{
			System.out.println("PArty ID"+pieChart.getPartyId());
			List clientName=new ArrayList();
			clientName=clientService.getClientName(pieChart.getClient().getClientId());
			HashMap<String, List<Object>> map=advReportService.chanelWiseAdvPieChart(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPartyId());
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
           	 List<Object> advList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
			 pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			 pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			 model.addAttribute("advList",advList);
			 model.addAttribute("ChannelList",ChannelList);
			 model.addAttribute("pieChart", pieChart);
			 model.addAttribute("clientName",clientName);
			 model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
           	session = request.getSession();
           	session.setAttribute("advList",advList);
			}
		    return "generateChannelWiseAdvPieChart";
		}
		if(param!=null && param.equals("Channel1"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			pieChart.setFromDate(DateFormat.getYYYYMMDDDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getYYYYMMDDDate(pieChart.getToDate()));
			System.out.println("clientID"+clientId);
			System.out.println("from"+pieChart.getFromDate());
			System.out.println("todtae"+pieChart.getToDate());
			System.out.println("party"+pieChart.getPartyId());
			HashMap<String, List<Object>> map=advReportService.chanelWiseAdvPieChart(clientId,pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPartyId());
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
				
           	 List<Object> advList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	 pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			 pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
           	 model.addAttribute("advList",advList);
			 model.addAttribute("ChannelList",ChannelList);
			 model.addAttribute("pieChart", pieChart);
			 model.addAttribute("clientName",clientName);
			 model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
			}
		    return "generateChannelWiseAdvPieChart1";
		}
		if(param!=null && param.equals("Channel2"))
		{
			List clientName=new ArrayList();
			clientName=clientService.getClientName(clientId);
			pieChart.setFromDate(DateFormat.getYYYYMMDDDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getYYYYMMDDDate(pieChart.getToDate()));
			System.out.println("clientID"+clientId);
			System.out.println("from"+pieChart.getFromDate());
			System.out.println("todtae"+pieChart.getToDate());
			System.out.println("party"+pieChart.getPartyId());
			HashMap<String, List<Object>> map=advReportService.chanelWiseAdvPieChart(clientId,pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPartyId());
			Set set = map.keySet();
			Iterator iterator=set.iterator();
			while(iterator.hasNext())
			{
				
           	 List<Object> advList= (List<Object>)map.get(iterator.next());
           	 List<Object> ChannelList= (List<Object>)map.get(iterator.next());
           	 pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			 pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
           	 model.addAttribute("advList",advList);
			 model.addAttribute("ChannelList",ChannelList);
			 model.addAttribute("pieChart", pieChart);
			 model.addAttribute("clientName",clientName);
			 model.addAttribute("todate",todate);
           	 model.addAttribute("clientId",clientId);
           	 model.addAttribute("fromdate",fromdate);
			}
		    return "generateChannelWiseAdvPieChart";
		}
		return "advPieChart";
	}
	
/* Advertisement PDF Report*/
	@RequestMapping("/AdvPDFReport")
	public String showAdvPDFReport(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Chanel> chanelList=new ArrayList<Chanel>();
		chanelList=chanelService.getAllChanel();
		if(chanelList!=null)
		{
			model.addAttribute("chanel", chanelList);
		}
		return "AdvPDFReport";
	}
	@RequestMapping(value = "/ADVPDFReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String genrateAdvPDFReport(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ) 
	{
		pieChartValidator.validate(pieChart, result);
		if(result.hasErrors())
		{
			return "AdvPDFReport";
		}
		@SuppressWarnings("rawtypes")
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		@SuppressWarnings("rawtypes")
		List channelName=new ArrayList();
		channelName=chanelService.getChannelName(pieChart.getChanel().getChannelId());
		System.out.println("Advertisement PDF Report");
		List<AdvertisementTracking> advertisementTracking=new ArrayList<AdvertisementTracking>();
		advertisementTracking=advReportService.advPDFReport(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getChanel().getChannelId());
		if(advertisementTracking!=null)
		{
			model.addAttribute("advertisementTracking", advertisementTracking);
			model.addAttribute("clientName", clientName);
			model.addAttribute("pieChart", pieChart);
			model.addAttribute("channelName", channelName);
		}
		System.out.println("advertisementTracking---"+advertisementTracking);
		return "generateAdvPDFReport";
	}
	
	
	@RequestMapping("/printPieChart")
	public String showprintPieChart(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Publication> publication = new ArrayList<Publication>();
		publication = printReportService.getAllPublication();
		if (publication != null) {
			model.addAttribute("publication", publication);
		}
		return "printPieChart";
	}
	
	@RequestMapping(value = "/PrintPieChartReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String genratePrintReport(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ) 
	{
		@SuppressWarnings("rawtypes")
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		if(param!=null && param.equals("{param=AllPaper}"))
		{
			HashMap<String, List<Object>> publicationMap=printReportService.AllpublicationPieChart(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),null,param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			@SuppressWarnings("rawtypes")
			Set  set = publicationMap.keySet();
            @SuppressWarnings("rawtypes")
			Iterator  it = set.iterator();
            while(it.hasNext())
            {
           	 List<Object>publicationNegitiveList = (List<Object>)publicationMap.get(it.next());
           	 List<Object>publicationList = (List<Object>)publicationMap.get(it.next());
           	 List<Object> publicationPositiveList= (List<Object>)publicationMap.get(it.next());
           	 model.addAttribute("publicationPositiveList",publicationPositiveList);
           	 model.addAttribute("publicationNegitiveList",publicationNegitiveList);
           	 model.addAttribute("publicationList",publicationList);
        	 model.addAttribute("pieChart",pieChart);
        	 model.addAttribute("clientName",clientName);
            }
            return "generateAllChannelPrintPieChart";
		}
		if(param!=null && param.equals("{param=Paper}"))
		{
			HashMap<String, List<Object>> publicationMap=printReportService.AllpublicationPieChart(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPublication().getPublicationId(),param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			@SuppressWarnings("rawtypes")
			Set  set = publicationMap.keySet();
            @SuppressWarnings("rawtypes")
			Iterator  it = set.iterator();
            while(it.hasNext())
            {
           	 List<Object>publicationNegitiveList = (List<Object>)publicationMap.get(it.next());
           	 List<Object>publicationList = (List<Object>)publicationMap.get(it.next());
           	 List<Object> publicationPositiveList= (List<Object>)publicationMap.get(it.next());
           	 model.addAttribute("publicationPositiveList",publicationPositiveList);
           	 model.addAttribute("publicationNegitiveList",publicationNegitiveList);
           	 model.addAttribute("publicationList",publicationList);
           	model.addAttribute("pieChart",pieChart);
            model.addAttribute("clientName",clientName);
            }
            return "generateAllChannelPrintPieChart";
		}
		if(param!=null && param.equals("{param=Sector}"))
		{
			HashMap<String, List<Object>> sectorMap=printReportService.sectorWiseAllPublication(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),null,param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			@SuppressWarnings("rawtypes")
			Set  set = sectorMap.keySet();
            @SuppressWarnings("rawtypes")
			Iterator  it = set.iterator();
            while(it.hasNext())
            {
           	 List<Object>sectorNegitiveList = (List<Object>)sectorMap.get(it.next());
           	 List<Object>publicationList = (List<Object>)sectorMap.get(it.next());
           	 List<Object> sectorPositiveList= (List<Object>)sectorMap.get(it.next());
           	 model.addAttribute("sectorPositiveList",sectorPositiveList);
           	 model.addAttribute("sectorNegitiveList",sectorNegitiveList);
           	 model.addAttribute("publicationList",publicationList);
           	 model.addAttribute("pieChart",pieChart);
           	 model.addAttribute("clientName",clientName);
           	
            }
			return "generateSectorWiseAllPublicationPieChart";
		}
		if(param!=null && param.equals("{param=PaperSector}"))
		{
			HashMap<String, List<Object>> sectorMap=printReportService.sectorWiseAllPublication(pieChart.getClient().getClientId(),pieChart.getFromDate(),pieChart.getToDate(),pieChart.getPublication().getPublicationId(),param);
			pieChart.setFromDate(DateFormat.getDDMMYYYYDate(pieChart.getFromDate()));
			pieChart.setToDate(DateFormat.getDDMMYYYYDate(pieChart.getToDate()));
			@SuppressWarnings("rawtypes")
			Set  set = sectorMap.keySet();
            @SuppressWarnings("rawtypes")
			Iterator  it = set.iterator();
            while(it.hasNext())
            {
           	 List<Object>sectorNegitiveList = (List<Object>)sectorMap.get(it.next());
           	 List<Object>publicationList = (List<Object>)sectorMap.get(it.next());
           	 List<Object> sectorPositiveList= (List<Object>)sectorMap.get(it.next());
           	 model.addAttribute("sectorPositiveList",sectorPositiveList);
           	 model.addAttribute("sectorNegitiveList",sectorNegitiveList);
           	 model.addAttribute("publicationList",publicationList);
           	 model.addAttribute("pieChart",pieChart);
           	 model.addAttribute("clientName",clientName);
            }
			return "generateSectorWiseAllPublicationPieChart";
		}
		return "printPieChart";
	}
	
	@RequestMapping("/printPDFReport")
	public String showprintPDFReport(Map<String, Object> map,Model model)
	{
		map.put("PieChart",new PieChart());
		List<Client> clientList=new ArrayList<Client>();
		clientList=clientService.getAllClientName();
		if(clientList!=null)
		{
			model.addAttribute("client", clientList);
		}
		List<Publication> publication = new ArrayList<Publication>();
		publication = printReportService.getAllPublication();
		if (publication != null) {
			model.addAttribute("publication", publication);
		}
		
		return "printPDFReport";
	}
	@RequestMapping(value = "/PrintPDFReport", method = {RequestMethod.GET,RequestMethod.POST })
	public String printPDF(@ModelAttribute("PieChart") PieChart pieChart,BindingResult result ,ModelMap  model
			,HttpServletRequest request,@RequestParam(required=false) String param ) 
	{
		@SuppressWarnings("rawtypes")
		List clientName=new ArrayList();
		clientName=clientService.getClientName(pieChart.getClient().getClientId());
		@SuppressWarnings("rawtypes")
		List publicationName=new ArrayList();
		publicationName=publicationService.getPublicationName(pieChart.getPublication().getPublicationId());
		List<PrintTracking> printTracking=new ArrayList<PrintTracking>();
		printTracking=printReportService.printPDFReport(pieChart.getFromDate(),pieChart.getToDate(),pieChart.getClient().getClientId(),pieChart.getPublication().getPublicationId());
		if (printTracking != null) {
			model.addAttribute("printTracking", printTracking);
			model.addAttribute("pieChart", pieChart);
			model.addAttribute("clientName", clientName);
			model.addAttribute("publicationName", publicationName);
		}
		System.out.println("Print Chart Report"+param);
		return "generatePrintPDFReport";
	}
	
}


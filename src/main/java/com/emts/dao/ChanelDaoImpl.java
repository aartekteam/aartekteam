package com.emts.dao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import com.emts.model.Chanel;
import com.emts.model.State;
import com.emts.util.IConstant;
@Repository
public class ChanelDaoImpl implements ChanelDao{
	@Autowired
	private HibernateTemplate hibernateTemplate;
	public void addChanelName(Chanel chanel) {
		State state=(State)hibernateTemplate.get(State.class, chanel.getState().getStateId());
		chanel.setState(state);
		hibernateTemplate.saveOrUpdate(chanel);
	}
	@SuppressWarnings("unchecked")
	public List<Object> getContent() {
		List<Object> chanelContent=null;
		chanelContent=hibernateTemplate.find("from Chanel where IS_DELETED="+IConstant.IS_DELETED_ACTIVE);
		return chanelContent;
	}
	public void removeContent(Integer Id) {
		Chanel chanel=(Chanel)hibernateTemplate.get(Chanel.class, Id);
		chanel.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
		if(null!=chanel)
		{
			hibernateTemplate.update(chanel);
		}
	}
	@SuppressWarnings("unchecked")
	public List<Object> updateChanel(Integer Id) {
		List<Object> chanel=null;
		chanel=hibernateTemplate.find("from Chanel chanel where chanel.channelId=?",Id );
		return chanel;
	}
	@SuppressWarnings("rawtypes")
	public List getChannelName(Integer channelId) {
		List channelName=null;
		channelName=hibernateTemplate.find("select channelName from Chanel  where IS_DELETED="+IConstant.IS_DELETED_ACTIVE+"and channelId="+channelId+" ");
		System.out.println("channelName"+channelName);
		System.out.println("channelName Dao");
		return channelName;
	}
	@SuppressWarnings("unchecked")
	public List<Object> getAllChanel() {
		List<Object> chanelList=null;
		chanelList=hibernateTemplate.find("from Chanel where IS_DELETED="+IConstant.IS_DELETED_ACTIVE);
		return chanelList;
	}
	@SuppressWarnings("unchecked")
	public List<Object> getChannelByState(Integer stateId) {
		List<Object> channelList=null;
		channelList=hibernateTemplate.find("from Chanel where IS_DELETED="+IConstant.IS_DELETED_ACTIVE+"and state.stateId="+stateId+" ");
		Chanel chanel = new Chanel();
		chanel=(Chanel)channelList.get(0);
		System.out.println("stateList"+chanel.getChannelId());
		System.out.println("stateList"+chanel.getChannelName());
		return channelList;
	}
}

package com.emts.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.emts.model.Country;
import com.emts.model.State;
import com.emts.util.IConstant;
@Repository
public class StateDaoImpl implements StateDao{
	@Autowired
	private HibernateTemplate hibernateTemplate;
	public void addStateName(State state) {
		Country country=(Country)hibernateTemplate.get(Country.class,state.getCountry().getCountryId());
		state.setCountry(country);
		hibernateTemplate.saveOrUpdate(state);
	}
	@SuppressWarnings("unchecked")
	public List<Object> getContent() {
		List<Object> stateContent=null;
		stateContent=hibernateTemplate.find("from State where IS_DELETED="+IConstant.IS_DELETED_ACTIVE);
		return stateContent;
	}
	public void removeContent(Integer Id) {
		State state=(State)hibernateTemplate.get(State.class ,Id);
		state.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
		if(null!=state)
		{
			hibernateTemplate.update(state);
		}
	}
	@SuppressWarnings("unchecked")
	public List<Object> updateState(Integer Id) {
		List<Object> state=null;
		 state=hibernateTemplate.find("from State state where state.stateId=?",Id);
		return state;
	}
	@SuppressWarnings("unchecked")
	public List<Object> getStateByCountry(Integer countryId) {
		List<Object> stateList=null;
		stateList=hibernateTemplate.find("from State where IS_DELETED="+IConstant.IS_DELETED_ACTIVE+"and country.countryId="+countryId+" ");
		State state =new State();
		state=(State) stateList.get(0);
		System.out.println("stateList"+state.getStateId());
		System.out.println("stateList"+state.getStateName());
		return stateList;
	}
	@SuppressWarnings("unchecked")
	public List<Object> getAllStateName() {
		List<Object> stateList=null;
		stateList=hibernateTemplate.find("from State where IS_DELETED="+IConstant.IS_DELETED_ACTIVE);
		return stateList;
	}

}

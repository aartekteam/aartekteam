package com.emts.dao;
import java.util.List;
import com.emts.model.City;
public interface CityDao {
	public void addcityName(City city);
	public List<Object> getContent();
	public void removeContent(Integer Id);
	public List<Object> updateCity(Integer Id);
	public List<Object> getAllCity();
	public List<Object> getcityByState(Integer stateId);
}

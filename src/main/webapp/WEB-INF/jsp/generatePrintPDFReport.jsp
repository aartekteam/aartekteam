<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<jsp:useBean id="date" class="java.util.Date" />
<html>
<head>
<link href="css/displaytag.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
<tr>
<td>
<img border="0" src="images/logo_inner_horizantel.png" alt="Pulpit rock" width="250" height="55" >
 <td>Client Name:<c:out value='${clientName.get(0)}' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td> 
<td>Publication Name:<b><c:out value='${publicationName.get(0)}' /></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b></b></td> 

<td>Date:<fmt:formatDate value="${date}" pattern="dd/mm/yyyy" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
</tr>
<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
</table>
	<display:table name="printTracking" pagesize="5" class="mid_tbl" export="true" requestURI="PrintPDFReport.do" >
		<display:column property="printTrackingId" title="S.NO" sortable="true" style="width:80px" />
		<display:column property="textArea" title="Slug" style="width:350px " />
		<!-- Check for Reference values -->
		<display:column property="pageNumber" title="Page" style="width:80px "/>
		 <display:column property="city.cityName" title="City" style="width:100px "/> 
		<display:column property="newsColumn" title="News Size(in Column)" style="width:100px "/> 
		 <display:column property="photo" title="With photo" />
		  <display:column property="sector.sectorName" title="Sector" style="width:150px "/>
		 <display:column property="subSector.subSectorName" title="Sub Sector" style="width:150px "/>
		<display:column property="newsTrend" title="News Trend" />
		<display:column property="storyCode" title="Story Code" />
		<display:setProperty name="export.pdf" value="true" />
	</display:table>
	<table align="right">	
																 <tr>
                                                                  <td align="right">
																 <img border="0" src="images/images.jpg" alt="Pulpit rock" width="70" height="70">
																 </td>
																 </tr>
                                                                </table>
<table class="buttom"><tr> <td > <input class="noContentPrint" type="button" value="Print" onclick="window.print()" /> </td> </tr> </table>
	
</body>
</html>
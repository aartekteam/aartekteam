<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<c:if  test="${!empty trackingStoryCodeList}">
		<table border="0" cellspacing="1" cellpadding="1" class="mid_tbl" width="100%">
		<tr><td><td></td><font size="5"><b>Search Result Based on Story code....</b></font></td></tr>
		<tr>
	<th>S.NO</th>
	<th>Slug</th>
	<th>City</th>
	<th>Time</th>
	<th>News Type</th>
	<th>Sector</th>
	<th>Sub Sector</th>
	<th>News Trend</th>
	<th>Story Code</th>
	</tr>
	<c:forEach items="${trackingStoryCodeList}" var="content">
	<tr>
	<td>${content.trackingId}</td>
	<td width="300">${content.textArea}</td>
	<td>${content.city.cityName}</td>
	<td>${content.time}</td>
	<td>${content.newsType.newsTypeName}</td>
	<td>${content.sector.sectorName}</td>
	<td>${content.subSector.subSectorName}</td>
	<td>${content.newsTrend}</td>
	<td>${content.storyCode}</td>
	</tr>
	</c:forEach>
	</table>
	</c:if>
	<c:if  test="${!empty publicationStoryCodeList}">
	<table border="0" cellspacing="1" cellpadding="1" class="mid_tbl" width="100%">
	<tr><td><td></td><font size="5"><b>Search Result Based on Story code....</b></font></td></tr>
	<tr>
	<th>S.NO</th>
	<th>Slug</th>
	<th>Page</th>
	<th>City</th>
	<th>News Size(in Column)</th>
	<th>With photo</th>
	<th>Sector</th>
	<th>Sub Sector</th>
	<th>News Trend</th>
	<th>Story Code</th>
	</tr>
	<c:forEach items="${publicationStoryCodeList}" var="content">
	<tr>
	<td>${content.printTrackingId}</td>
	<td width="300">${content.textArea}</td>
	<td>${content.pageNumber}</td>
	<td>${content.city.cityName}</td>
	<td>${content.newsColumn}</td>
	<td>${content.photo}</td>
	<td>${content.sector.sectorName}</td>
	<td>${content.subSector.subSectorName}</td>
	<td>${content.newsTrend}</td>
	<td>${content.storyCode}</td>
	</tr>
	</c:forEach>
	</table>
	</c:if>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr><td valign="top" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"  >
						<tr><td valign="top" align="center"><img src="images/images.jpg" width="546" height="528" /></td></tr></table></td>
					</tr>
	</table>
</body>
</html>

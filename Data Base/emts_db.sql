/*
Navicat MySQL Data Transfer

Source Server         : EMTS
Source Server Version : 50130
Source Host           : localhost:3306
Source Database       : emts_db

Target Server Type    : MYSQL
Target Server Version : 50130
File Encoding         : 65001

Date: 2013-11-10 11:58:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `ADVERTISEMENT_ID` int(30) NOT NULL AUTO_INCREMENT,
  `SLUG` varchar(1000) DEFAULT NULL,
  `ADV_TYPE_ID` int(30) DEFAULT NULL,
  `PARTY_ID` int(30) DEFAULT NULL,
  `DURATION` varchar(50) DEFAULT NULL,
  `CLIENT_ID` int(30) DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(50) DEFAULT NULL,
  `CITY_ID` int(50) DEFAULT NULL,
  `END_TIME` varchar(255) DEFAULT NULL,
  `START_TIME` varchar(255) DEFAULT NULL,
  `CHANNEL_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ADVERTISEMENT_ID`),
  KEY `ADV_TYPE_ID` (`ADV_TYPE_ID`),
  KEY `PARTY_ID` (`PARTY_ID`),
  KEY `ADV_STATE_ID` (`STATE_ID`),
  KEY `ADV_CLIENT_ID` (`CLIENT_ID`),
  KEY `ADV_CITY_ID` (`CITY_ID`),
  KEY `FKF85DD20534CEC20F` (`CHANNEL_ID`),
  CONSTRAINT `ADV_CHANNEL_ID` FOREIGN KEY (`CHANNEL_ID`) REFERENCES `channel` (`CHANNEL_ID`),
  CONSTRAINT `ADV_CITY_ID` FOREIGN KEY (`CITY_ID`) REFERENCES `city` (`CITY_ID`),
  CONSTRAINT `ADV_CLIENT_ID` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`CLIENT_ID`),
  CONSTRAINT `ADV_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `party` (`PARTY_ID`),
  CONSTRAINT `ADV_STATE_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`),
  CONSTRAINT `ADV_TYPE_ID` FOREIGN KEY (`ADV_TYPE_ID`) REFERENCES `advtype` (`ADV_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advertisement
-- ----------------------------
INSERT INTO `advertisement` VALUES ('1', 'hhhhh', '1', '1', '10', '4', '2', '2013-05-05', null, null, null, null, '1', null, '5:20', '5:10', null);
INSERT INTO `advertisement` VALUES ('2', 'hiii', '1', '1', '25', '4', '3', '2013-06-06', null, null, null, null, '1', '3', '11:45', '11:20', '4');
INSERT INTO `advertisement` VALUES ('3', 'hiii', '1', '1', '25', '4', '3', '2013-06-06', null, null, null, null, '0', '3', '11:45', '11:20', '4');

-- ----------------------------
-- Table structure for advtype
-- ----------------------------
DROP TABLE IF EXISTS `advtype`;
CREATE TABLE `advtype` (
  `ADV_TYPE_ID` int(30) NOT NULL AUTO_INCREMENT,
  `ADV_TYPE_NAME` varchar(50) DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(30) DEFAULT NULL,
  PRIMARY KEY (`ADV_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advtype
-- ----------------------------
INSERT INTO `advtype` VALUES ('1', 'Paid News', null, null, null, null, '1');
INSERT INTO `advtype` VALUES ('2', 'dfgfghfg', null, null, null, null, '1');

-- ----------------------------
-- Table structure for channel
-- ----------------------------
DROP TABLE IF EXISTS `channel`;
CREATE TABLE `channel` (
  `CHANNEL_ID` int(30) NOT NULL AUTO_INCREMENT,
  `CHANNEL_NAME` varchar(50) DEFAULT NULL,
  `CHANNEL_SHORT_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`CHANNEL_ID`),
  KEY `CHANEEL_STATE_ID` (`STATE_ID`),
  CONSTRAINT `CHANNEL_STATE_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of channel
-- ----------------------------
INSERT INTO `channel` VALUES ('1', 'SaharaMP', 'SHMP', null, null, null, null, '3', '0');
INSERT INTO `channel` VALUES ('2', 'Sahara MP', 'SHMP', null, null, null, null, '2', '1');
INSERT INTO `channel` VALUES ('3', 'ZeeTv', 'ZT', null, null, null, null, '2', '1');
INSERT INTO `channel` VALUES ('4', 'Star Gold', 'SG', null, null, null, null, '3', '1');
INSERT INTO `channel` VALUES ('5', 'dsff', 'dfs', null, null, null, null, '2', '1');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `CITY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CITY_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`CITY_ID`),
  KEY `CITY_ID` (`STATE_ID`),
  CONSTRAINT `CITY_STATE_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', 'khandwav', null, null, null, null, '3', '0');
INSERT INTO `city` VALUES ('2', 'khandwa', null, null, null, null, '3', '0');
INSERT INTO `city` VALUES ('3', 'indore', null, null, null, null, '3', '1');
INSERT INTO `city` VALUES ('4', 'ghgh', null, null, null, null, '4', '1');
INSERT INTO `city` VALUES ('5', 'popfds', null, null, null, null, '5', '1');
INSERT INTO `city` VALUES ('6', 'xyz', null, null, null, null, '6', '1');

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `CLIENT_ID` int(30) NOT NULL AUTO_INCREMENT,
  `CLIENT_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(30) DEFAULT NULL,
  PRIMARY KEY (`CLIENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'Praveeno', null, null, null, null, '0');
INSERT INTO `client` VALUES ('2', 'sdf', null, null, null, null, '0');
INSERT INTO `client` VALUES ('3', 'fdsf', null, null, null, null, '0');
INSERT INTO `client` VALUES ('4', 'sumit12', null, null, null, null, '1');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `COUNTRY_ID` int(30) NOT NULL AUTO_INCREMENT,
  `COUNTRY_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`COUNTRY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'New  York1', null, null, null, null, '0');
INSERT INTO `country` VALUES ('2', 'india', null, null, null, null, '0');
INSERT INTO `country` VALUES ('3', 'india', null, null, null, null, '0');
INSERT INTO `country` VALUES ('4', 'india', null, null, null, null, '0');
INSERT INTO `country` VALUES ('5', 'Pakistan', null, null, null, null, '0');
INSERT INTO `country` VALUES ('6', 'Aus', null, null, null, null, '0');
INSERT INTO `country` VALUES ('7', 'hhh', null, null, null, null, '0');
INSERT INTO `country` VALUES ('8', 'fdfgdfg', null, null, null, null, '1');
INSERT INTO `country` VALUES ('9', 'fdfgdfg', null, null, null, null, '0');
INSERT INTO `country` VALUES ('10', 'hgjg', null, null, null, null, '0');
INSERT INTO `country` VALUES ('11', 'india', null, null, null, null, '1');
INSERT INTO `country` VALUES ('12', 'india', null, null, null, null, '1');
INSERT INTO `country` VALUES ('13', 'Pakistan', null, null, null, null, '1');
INSERT INTO `country` VALUES ('14', 'Pakistan', null, null, null, null, '1');
INSERT INTO `country` VALUES ('15', 'cvbcb', null, null, null, null, '1');
INSERT INTO `country` VALUES ('16', 'dsffdffgh', null, null, null, null, '0');

-- ----------------------------
-- Table structure for newstype
-- ----------------------------
DROP TABLE IF EXISTS `newstype`;
CREATE TABLE `newstype` (
  `NEWS_TYPE_ID` int(30) NOT NULL AUTO_INCREMENT,
  `NEWS_TYPE_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`NEWS_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newstype
-- ----------------------------
INSERT INTO `newstype` VALUES ('1', 'Anchor Visual', null, null, null, null, '1');
INSERT INTO `newstype` VALUES ('2', 'Anchor Visual Bite', null, null, null, null, '1');
INSERT INTO `newstype` VALUES ('3', 'Talk Show', null, null, null, null, '1');
INSERT INTO `newstype` VALUES ('4', 'Package', null, null, null, null, '1');
INSERT INTO `newstype` VALUES ('5', 'Live', null, null, null, null, '1');

-- ----------------------------
-- Table structure for party
-- ----------------------------
DROP TABLE IF EXISTS `party`;
CREATE TABLE `party` (
  `PARTY_ID` int(30) NOT NULL AUTO_INCREMENT,
  `PARTY_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(30) DEFAULT NULL,
  PRIMARY KEY (`PARTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of party
-- ----------------------------
INSERT INTO `party` VALUES ('1', 'Congressss', null, null, null, null, '1');
INSERT INTO `party` VALUES ('2', 'BJP', null, null, null, null, '0');
INSERT INTO `party` VALUES ('3', 'bjp', null, null, null, null, '0');

-- ----------------------------
-- Table structure for popup
-- ----------------------------
DROP TABLE IF EXISTS `popup`;
CREATE TABLE `popup` (
  `POP_ID` int(30) NOT NULL,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `IS_DELETED` int(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  PRIMARY KEY (`POP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of popup
-- ----------------------------

-- ----------------------------
-- Table structure for printtracking
-- ----------------------------
DROP TABLE IF EXISTS `printtracking`;
CREATE TABLE `printtracking` (
  `PRINT_TRACKING_ID` int(30) NOT NULL,
  `SLUG` varchar(1000) DEFAULT NULL,
  `PAGE` varchar(50) DEFAULT NULL,
  `NEWS_COLUMN` varchar(50) DEFAULT NULL,
  `CITY_ID` int(30) DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  `SUB_SECTOR_ID` int(30) DEFAULT NULL,
  `NEWS_TREND` varchar(50) DEFAULT NULL,
  `STORY_CODE` varchar(50) DEFAULT NULL,
  `MARKING` varchar(50) DEFAULT NULL,
  `PUBLICATION_ID` int(30) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `CLIENT_ID` int(30) DEFAULT NULL,
  `SECTOR_ID` int(30) DEFAULT NULL,
  `PHOTO_COLUMN` varchar(30) DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  PRIMARY KEY (`PRINT_TRACKING_ID`),
  KEY `PRINT_CITY_ID` (`CITY_ID`),
  KEY `PRINT_STATE_ID` (`STATE_ID`),
  KEY `PUBLICATION_ID` (`PUBLICATION_ID`),
  KEY `PRINT_SECTOR_ID` (`SECTOR_ID`),
  KEY `PRINT_SUB_SECTOR_ID` (`SUB_SECTOR_ID`),
  KEY `PRINT_CLIENT_ID` (`CLIENT_ID`),
  CONSTRAINT `PRINT_CITY_ID` FOREIGN KEY (`CITY_ID`) REFERENCES `city` (`CITY_ID`),
  CONSTRAINT `PRINT_CLIENT_ID` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`CLIENT_ID`),
  CONSTRAINT `PRINT_SECTOR_ID` FOREIGN KEY (`SECTOR_ID`) REFERENCES `sector` (`SECTOR_ID`),
  CONSTRAINT `PRINT_STATE_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`),
  CONSTRAINT `PRINT_SUB_SECTOR_ID` FOREIGN KEY (`SUB_SECTOR_ID`) REFERENCES `subsector` (`SUB_SECTOR_ID`),
  CONSTRAINT `PUBLICATION_ID` FOREIGN KEY (`PUBLICATION_ID`) REFERENCES `publication` (`PUBLICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of printtracking
-- ----------------------------

-- ----------------------------
-- Table structure for publication
-- ----------------------------
DROP TABLE IF EXISTS `publication`;
CREATE TABLE `publication` (
  `PUBLICATION_ID` int(30) NOT NULL AUTO_INCREMENT,
  `PUBLICATION_NAME` varchar(50) DEFAULT NULL,
  `PUBLICATION_SHORT_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  PRIMARY KEY (`PUBLICATION_ID`),
  KEY `STATED_ID` (`STATE_ID`),
  CONSTRAINT `STATED_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of publication
-- ----------------------------
INSERT INTO `publication` VALUES ('1', 'dainik bhaskar', 'DBK', null, null, null, null, '1', '3');
INSERT INTO `publication` VALUES ('2', 'dainik bhaskaryhd', 'DBK', null, null, null, null, '0', '3');
INSERT INTO `publication` VALUES ('3', 'ges', 'etyse', null, null, null, null, '0', '2');
INSERT INTO `publication` VALUES ('4', 'gdxx', 'hgdhyxd', null, null, null, null, '0', '2');

-- ----------------------------
-- Table structure for registration
-- ----------------------------
DROP TABLE IF EXISTS `registration`;
CREATE TABLE `registration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `CONTACT` double(50,0) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `SEX` varchar(255) DEFAULT NULL,
  `IS_DELETED` int(11) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of registration
-- ----------------------------
INSERT INTO `registration` VALUES ('1', 'Palasiya', 'ajay@gmail.com', 'Ajay', 'jirati', '999999999', '123', 'M', '1', '1', '2013-11-08', '1', '2013-11-08');
INSERT INTO `registration` VALUES ('2', 'Saket Nagar', 'praveen@gmail.com', 'Praveen', 'Raghuvanshi', '78', '123', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('3', 'saket', 'sumit@gmail.com', 'sumit', 'gangrade', '780', '789', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('4', 'saket', 'sumit@gmail.com', 'sumit', 'gangrade', '7801222', '12', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('5', 'saket', 'sumit@gmail.com', 'sumit', 'gangrade', '7801222', '12', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('6', 'dfga', '1@gmail.com', 'abc', 'gangrade', '7804', '563', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('7', 'dfga', '1@gmail.com', 'abc', 'gangrade', '88877554', '45', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('8', 'saket', 'ksd@gmail.com', 'jkj', 'jkj', '7804814213', '456', 'Male', '1', null, null, null, null);
INSERT INTO `registration` VALUES ('9', 'saket', 'ksd@gmail.com', 'jkj', 'jkj', '7804814213', '456', 'Male', '1', null, null, null, null);

-- ----------------------------
-- Table structure for sector
-- ----------------------------
DROP TABLE IF EXISTS `sector`;
CREATE TABLE `sector` (
  `SECTOR_ID` int(30) NOT NULL AUTO_INCREMENT,
  `SECTOR_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`SECTOR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sector
-- ----------------------------
INSERT INTO `sector` VALUES ('1', 'Businesseee', null, null, null, null, '0');
INSERT INTO `sector` VALUES ('2', 'crime', null, null, null, null, '1');

-- ----------------------------
-- Table structure for state
-- ----------------------------
DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `STATE_ID` int(30) NOT NULL AUTO_INCREMENT,
  `STATE_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `COUNTRY_ID` int(30) DEFAULT NULL,
  `IS_DELETED` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATE_ID`),
  KEY `STATE_ID` (`COUNTRY_ID`),
  CONSTRAINT `STATE_COUNTRY_ID` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `country` (`COUNTRY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of state
-- ----------------------------
INSERT INTO `state` VALUES ('1', 'mpp', null, null, null, null, null, '0');
INSERT INTO `state` VALUES ('2', 'UP', null, null, null, null, '2', '1');
INSERT INTO `state` VALUES ('3', 'M.P', null, null, null, null, '2', '0');
INSERT INTO `state` VALUES ('4', 'fdgf', null, null, null, null, null, '1');
INSERT INTO `state` VALUES ('5', 'karachi', null, null, null, null, '5', '1');
INSERT INTO `state` VALUES ('6', 'siddeny', null, null, null, null, '6', '1');

-- ----------------------------
-- Table structure for subsector
-- ----------------------------
DROP TABLE IF EXISTS `subsector`;
CREATE TABLE `subsector` (
  `SUB_SECTOR_ID` int(30) NOT NULL AUTO_INCREMENT,
  `SUB_SECTOR_NAME` varchar(50) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  PRIMARY KEY (`SUB_SECTOR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subsector
-- ----------------------------
INSERT INTO `subsector` VALUES ('1', 'abcxyz', null, null, null, null, '1');
INSERT INTO `subsector` VALUES ('2', 'xyz', null, null, null, null, '0');

-- ----------------------------
-- Table structure for tracking
-- ----------------------------
DROP TABLE IF EXISTS `tracking`;
CREATE TABLE `tracking` (
  `TRACKING_ID` int(30) NOT NULL AUTO_INCREMENT,
  `SLUG` varchar(1000) DEFAULT NULL,
  `TIME` varchar(30) DEFAULT NULL,
  `NEWS_TYPE_ID` int(30) DEFAULT NULL,
  `CLIENT_ID` int(30) DEFAULT NULL,
  `SECTOR_ID` int(30) DEFAULT NULL,
  `CITY_ID` int(30) DEFAULT NULL,
  `SUB_SECTOR_ID` int(30) DEFAULT NULL,
  `NEWS_TREND` varchar(50) DEFAULT NULL,
  `STORY_CODE` varchar(50) DEFAULT NULL,
  `MARKING` int(100) DEFAULT NULL,
  `CHANNEL_ID` int(30) DEFAULT NULL,
  `STATE_ID` int(30) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `IS_DELETED` int(20) DEFAULT NULL,
  `CREATED_BY` varchar(50) DEFAULT NULL,
  `CREATED_DATE` date DEFAULT NULL,
  `UPDATED_BY` varchar(50) DEFAULT NULL,
  `UPDATED_DATE` date DEFAULT NULL,
  PRIMARY KEY (`TRACKING_ID`),
  KEY `CHANNEL_ID` (`CHANNEL_ID`),
  KEY `TRACKING_CITY_ID` (`CITY_ID`),
  KEY `CLIENT_ID` (`CLIENT_ID`),
  KEY `NEWS_TYPE_ID` (`NEWS_TYPE_ID`),
  KEY `TRACKING_STATE_ID` (`STATE_ID`),
  KEY `TRACKING_SECTOR_ID` (`SECTOR_ID`),
  KEY `SUB_SECTOR_ID` (`SUB_SECTOR_ID`),
  CONSTRAINT `CHANNEL_ID` FOREIGN KEY (`CHANNEL_ID`) REFERENCES `channel` (`CHANNEL_ID`),
  CONSTRAINT `CLIENT_ID` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`CLIENT_ID`),
  CONSTRAINT `NEWS_TYPE_ID` FOREIGN KEY (`NEWS_TYPE_ID`) REFERENCES `newstype` (`NEWS_TYPE_ID`),
  CONSTRAINT `SUB_SECTOR_ID` FOREIGN KEY (`SUB_SECTOR_ID`) REFERENCES `subsector` (`SUB_SECTOR_ID`),
  CONSTRAINT `TRACKING_CITY_ID` FOREIGN KEY (`CITY_ID`) REFERENCES `city` (`CITY_ID`),
  CONSTRAINT `TRACKING_SECTOR_ID` FOREIGN KEY (`SECTOR_ID`) REFERENCES `sector` (`SECTOR_ID`),
  CONSTRAINT `TRACKING_STATE_ID` FOREIGN KEY (`STATE_ID`) REFERENCES `state` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tracking
-- ----------------------------
INSERT INTO `tracking` VALUES ('2', 'Good Morning', '10:10', '1', '4', '2', null, '1', 'Positive', '45', '1', '4', '3', '2013-05-05', '1', null, null, null, null);
INSERT INTO `tracking` VALUES ('3', 'hiiii', '10', '1', '4', '2', '5', '1', 'Positive', '56', '1', '4', '6', '2013-11-09', '0', null, null, null, null);
